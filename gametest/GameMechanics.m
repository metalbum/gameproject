//
//  GameMechanics.m
//  gametest
//
//  Created by Anders on 2015-04-10.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "GameMechanics.h"
#import "Enemy.h"
#import "constants.h"

@implementation GameMechanics

-(void)moveCharacter:(Character *)character inDirection:(int)direction {
    character.isMoving = YES;
    character.currentDirection = direction;
}

-(void)stopCharacter:(Character *)character {
    character.isMoving = NO;
    [character.characterNode removeAllActions];
    
}

-(void)setUpDPad:(SKScene*)scene {
    
    self.leftSteer = [SKSpriteNode spriteNodeWithImageNamed:@"PadLeft"];
    self.leftSteer.position = CGPointMake(20 - ((scene.frame.size.width / 2) - (self.leftSteer.frame.size.width / 2)), 180 - ((scene.frame.size.height / 2) - (self.leftSteer.frame.size.height / 2)));
    self.leftSteer.zPosition = 1000;
    [scene addChild:self.leftSteer];
    
    
    self.rightSteer = [SKSpriteNode spriteNodeWithImageNamed:@"PadRight"];
    self.rightSteer.position = CGPointMake(self.leftSteer.position.x + 140, self.leftSteer.position.y);
    self.rightSteer.zPosition = 1000;
    [scene addChild:self.rightSteer];
    
    self.upSteer = [SKSpriteNode spriteNodeWithImageNamed:@"PadUp"];
    self.upSteer.position = CGPointMake(self.leftSteer.position.x + 70, self.leftSteer.position.y + 70);
    self.upSteer.zPosition = 1000;
    [scene addChild:self.upSteer];
    
    self.downSteer = [SKSpriteNode spriteNodeWithImageNamed:@"PadDown"];
    self.downSteer.position = CGPointMake(self.leftSteer.position.x + 70, self.leftSteer.position.y - 70);
    self.downSteer.zPosition = 1000;
    [scene addChild:self.downSteer];
    
}

-(void)addHero:(SKNode*)worldNode {
    self.hero = [Hero node];
    [worldNode addChild:self.hero];
    //[self debugPath:self.hero.characterNode];
}

//-(void)setUpHeroHealthBar:(SKScene*)scene {
//    
//    self.healthContainer = [SKSpriteNode spriteNodeWithImageNamed:@"LargeHealthContainer"];
//    self.healthContainer.zPosition = 1000;
//    self.healthContainer.position = CGPointMake(20 - ((scene.frame.size.width / 2) - (self.healthContainer.frame.size.width / 2)), 630 - ((scene.frame.size.height / 2) - (self.healthContainer.frame.size.height / 2)));
//    [scene addChild:self.healthContainer];
//    
//    self.healthBar = [SKSpriteNode spriteNodeWithImageNamed:@"LargeHealthBar"];
//    self.healthBar.zPosition = self.healthContainer.zPosition - 1;
//    self.healthBar.position = CGPointMake(self.healthContainer.position.x -(self.healthBar.frame.size.width / 2), self.healthContainer.position.y);
//    self.healthBar.anchorPoint = CGPointMake(0, 0.5);
//    [scene addChild:self.healthBar];
//    
//}

-(void)addEnemies:(SKNode*)worldNode withMin:(int)min andMax:(int)max {
    self.enemies = [[NSMutableArray alloc] init];
    for (int i = 0; i < arc4random_uniform(max) + min; i++) {
        Enemy *newEnemy = [Enemy node];
        int xPosition = (arc4random_uniform((worldNode.parent.frame.size.width)) - worldNode.parent.frame.size.width / 2);
        int yPosition = (arc4random_uniform((worldNode.parent.frame.size.height)) - worldNode.parent.frame.size.height / 2);
        newEnemy.position = CGPointMake(xPosition, yPosition);
        [worldNode addChild:newEnemy];
        [self.enemies addObject:newEnemy];
    }
    if (self.enemies.count == 1) {
        NSLog(@"There is 1 enemy in world");
    } else {
        NSLog(@"There are %d enemies in world", (int)self.enemies.count);
    }
}

//-(bool)withinDistance:(CGPoint)position ofTarget:(CGPoint)target threshold:(float)threshold {
//    
//    float x = position.x - target.x;
//    float y = position.y - target.y;
//    
//    return (x * x) * (y * y) < (threshold * threshold);
//}

-(void)centerOnNode:(SKNode *)node {
    CGPoint cameraPosition = [node.scene convertPoint:node.position fromNode:node.parent];
    node.parent.position = CGPointMake(node.parent.position.x - cameraPosition.x, node.parent.position.y - cameraPosition.y);
}

@end
