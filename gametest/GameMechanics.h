//
//  GameMechanics.h
//  gametest
//
//  Created by Anders on 2015-04-10.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorldState.h"
#import "Character.h"
#import "Hero.h"

@interface GameMechanics : NSObject

@property SKSpriteNode *leftSteer;
@property SKSpriteNode *rightSteer;
@property SKSpriteNode *upSteer;
@property SKSpriteNode *downSteer;
@property SKSpriteNode *healthContainer;
@property SKSpriteNode *healthBar;
@property Hero *hero;
@property NSMutableArray *enemies;

-(void)moveCharacter:(Character *)character inDirection:(int)direction;
-(void)stopCharacter:(Character *)character;
-(void)setUpDPad:(SKScene*)scene;
-(void)addHero:(SKNode*)worldNode;
//-(void)setUpHeroHealthBar:(SKScene*)scene;
-(void)addEnemies:(SKNode*)worldNode withMin:(int)min andMax:(int)max;
//-(bool)withinDistance:(CGPoint)position ofTarget:(CGPoint)target threshold:(float)threshold;
-(void)centerOnNode:(SKNode *)node;

@end
