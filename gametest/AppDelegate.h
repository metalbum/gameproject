//
//  AppDelegate.h
//  gametest
//
//  Created by Anders on 2015-04-05.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

