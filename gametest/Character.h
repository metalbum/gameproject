//
//  Character.h
//  gametest
//
//  Created by Anders on 2015-04-09.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Character : SKNode

@property SKSpriteNode *characterNode;
@property SKSpriteNode *swordSlash;
@property SKSpriteNode *healthContainer;
@property SKSpriteNode *healthBar;

@property NSString *characterName;
@property int level;
@property int HP;
@property int maxHealth;
@property int XP;
@property int strength;
@property int toughness;
@property int gold;
@property int potions;
@property int characterSpeed;
@property int range;
@property int currentDirection;
@property bool isDead;
@property bool isMoving;
@property int damageDealt;

@property SKAction *walkRightAction;
@property SKAction *walkLeftAction;
@property SKAction *walkUpAction;
@property SKAction *walkDownAction;
@property SKAction *changeTexture;

-(void)attackInDirection:(int)direction;
-(void)attack:(Character*)character;
-(void)takeDamage:(int) damageToTake fromDirection:(int) direction;
-(void)update;
-(SKAction*)setUpFramesWithAtlasNamed:(NSString*)atlasName andBaseName:(NSString*)baseName andNumberOframes:(int)numberOfFrames pingPong:(bool)pingPong;
-(void)setChangeTextureAction;

@end
