//
//  Enemy.h
//  gametest
//
//  Created by Anders on 2015-04-09.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Character.h"

@interface Enemy : Character

@property bool readyToStrike;
-(void)toggleReadyToStrike;

@end
