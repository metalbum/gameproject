//
//  GameScene.h
//  gametest
//

//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@end
