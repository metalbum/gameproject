
//  main.m
//  gametest
//
//  Created by Anders on 2015-04-05.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}