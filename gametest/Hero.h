//
//  Hero.h
//  gametest
//
//  Created by Anders on 2015-04-07.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Enemy.h"
#import "Character.h"

@interface Hero : Character

-(void)setUpHealthBar:(SKScene*)scene;

@end
