static const int none = 0;
static const int up = 1;
static const int down = 2;
static const int left = 3;
static const int right = 4;

static const int edgeOfMapCategory = 1;
static const int characterCategory = 2;
static const int enemyCategory = 3;
static const int swordCategory = 4;