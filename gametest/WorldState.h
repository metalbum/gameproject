//
//  WorldState.h
//  gametest
//
//  Created by Anders on 2015-04-23.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero.h"

@interface WorldState : NSObject

@property Hero *hero;

+(WorldState*)instance;

@end
