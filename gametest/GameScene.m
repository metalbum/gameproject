//
//  GameScene.m
//  gametest
//
//  Created by Anders on 2015-04-05.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "GameScene.h"
#import "WorldState.h"
#import "GameMechanics.h"
#import "constants.h"

@interface GameScene()

@property GameMechanics *gameMechanics;
@property SKNode *world;
@property UITouch *steerTouch;

@end

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    NSLog(@"Creating a brand new scene");
    
    self.gameMechanics = [[GameMechanics alloc] init];
    [self setUpWorld];
    [self.gameMechanics setUpDPad:self];
    [self.gameMechanics addHero:self.world];
    [self.gameMechanics.hero setUpHealthBar:self];
    [self.gameMechanics addEnemies:self.world withMin:1 andMax:5];
}

-(void)setUpWorld {
    self.anchorPoint = CGPointMake(0.5, 0.5);
    self.world = [SKNode node];
    [self addChild:self.world];
    SKSpriteNode *map = [SKSpriteNode spriteNodeWithImageNamed:@"firstMap"];
    map.position = CGPointMake(0,0);
    [self.world addChild:map];
    self.physicsWorld.gravity = CGVectorMake(0.0, 0.0);
    
    SKSpriteNode *topWall = [SKSpriteNode spriteNodeWithImageNamed:@"horizontalWallLong"];
    topWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:topWall.frame];
    topWall.position = CGPointMake(map.position.x, (map.frame.size.height / 2) + (topWall.size.height / 2));
    [self.world addChild:topWall];
    
    SKSpriteNode *bottomWall = [SKSpriteNode spriteNodeWithImageNamed:@"horizontalWallLong"];
    bottomWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:bottomWall.frame];
    bottomWall.position = CGPointMake(map.position.x, ((map.frame.size.height / 2) - (topWall.size.height / 2)) - map.size.height);
    [self.world addChild:bottomWall];
    
    SKSpriteNode *leftShortWall = [SKSpriteNode spriteNodeWithImageNamed:@"verticalWallShort"];
    leftShortWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:leftShortWall.frame];
    leftShortWall.position = CGPointMake(map.position.x - (map.frame.size.width / 2) + (leftShortWall.size.width / 2), (map.frame.size.height / 2) - (leftShortWall.size.height / 2));
    [self.world addChild:leftShortWall];
    
    SKSpriteNode *leftDoor = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(100, 200)];
    leftDoor.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:leftDoor.frame];
    leftDoor.position = CGPointMake(map.position.x - (map.frame.size.width / 2) - (leftShortWall.size.width / 2), leftShortWall.size.height + (leftDoor.size.height / 2));
    [self.world addChild:leftDoor];
    
    SKSpriteNode *leftLongWall = [SKSpriteNode spriteNodeWithImageNamed:@"verticalWallLong"];
    leftLongWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:leftLongWall.frame];
    leftLongWall.position = CGPointMake(map.position.x - (map.frame.size.width / 2) + (leftLongWall.size.width / 2), bottomWall.position.y + (leftLongWall.size.height / 2) + (bottomWall.size.height / 2));
    [self.world addChild:leftLongWall];
    
    SKSpriteNode *rightShortWall = [SKSpriteNode spriteNodeWithImageNamed:@"verticalWallShort"];
    rightShortWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:rightShortWall.frame];
    rightShortWall.position = CGPointMake(map.position.x + (map.frame.size.width / 2) - (rightShortWall.size.width / 2), bottomWall.position.y + (rightShortWall.size.height / 2) + (bottomWall.size.height / 2));
    [self.world addChild:rightShortWall];
    
    SKSpriteNode *rightLongWall = [SKSpriteNode spriteNodeWithImageNamed:@"verticalWallLong"];
    rightLongWall.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:rightLongWall.frame];
    rightLongWall.position = CGPointMake(map.position.x + (map.frame.size.width / 2) - (rightLongWall.size.width / 2), (map.frame.size.height / 2) - (rightLongWall.size.height / 2));
    [self.world addChild:rightLongWall];
    
    SKSpriteNode *rightDoor = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(100, 200)];
    rightDoor.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:rightDoor.frame];
    rightDoor.position = CGPointMake(map.position.x + (map.frame.size.width / 2) + (rightLongWall.size.width / 2), rightShortWall.size.height + (rightDoor.size.height / 2));
    [self.world addChild:rightDoor];
    
//    self.physicsWorld.contactDelegate = self;
//    self.world.physicsBody = [SKPhysicsBody bodyWithTexture:map.texture size:map.size];
//    self.world.physicsBody.categoryBitMask = edgeOfMapCategory;
//    self.world.physicsBody.dynamic = NO;
}

-(void)didBeginContact:(SKPhysicsContact *)contact {
    
}

-(void)didSimulatePhysics {
    [self.gameMechanics centerOnNode:self.gameMechanics.hero];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode: self];
    SKAction *changeTexture;
    
    if ([self.gameMechanics.leftSteer containsPoint:location]) {
        [self.gameMechanics.hero.characterNode runAction:self.gameMechanics.hero.walkLeftAction];
        [self.gameMechanics moveCharacter:self.gameMechanics.hero inDirection:left];
        self.steerTouch = touch;
    } else if ([self.gameMechanics.rightSteer containsPoint:location]) {
        [self.gameMechanics.hero.characterNode runAction:self.gameMechanics.hero.walkRightAction];
        [self.gameMechanics moveCharacter:self.gameMechanics.hero inDirection:right];
        self.steerTouch = touch;
    } else if ([self.gameMechanics.upSteer containsPoint:location]) {
        [self.gameMechanics.hero.characterNode runAction:self.gameMechanics.hero.walkUpAction];
        [self.gameMechanics moveCharacter:self.gameMechanics.hero inDirection:up];
        self.steerTouch = touch;
    } else if ([self.gameMechanics.downSteer containsPoint:location]) {
        [self.gameMechanics.hero.characterNode runAction:self.gameMechanics.hero.walkDownAction];
        [self.gameMechanics moveCharacter:self.gameMechanics.hero inDirection:down];
        self.steerTouch = touch;
    }
    [self.gameMechanics.hero.characterNode runAction:changeTexture];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch == self.steerTouch) {
        [self.gameMechanics stopCharacter:self.gameMechanics.hero];
        self.steerTouch = nil;
    } else {
        [self.gameMechanics.hero attackInDirection:self.gameMechanics.hero.currentDirection];
        for (Enemy *enemy in self.gameMechanics.enemies) {
            if ([self.gameMechanics.hero.swordSlash intersectsNode:enemy.characterNode]) {
                [enemy takeDamage:self.gameMechanics.hero.damageDealt fromDirection:self.gameMechanics.hero.currentDirection];
                if (enemy.isDead) {
                    [enemy removeFromParent];
                }
            }
        }
    }
    //Code to restart level when all enemies are dead, this will be removed later on...
    bool allEnemiesDead = NO;
    for (int i = 0; i < self.gameMechanics.enemies.count; i++) {
        if (![self.gameMechanics.enemies[i] isDead]) {
            allEnemiesDead = NO;
            break;
        } else {
            allEnemiesDead = YES;
        }
    }
    if (allEnemiesDead) {
        [self startNextLevel];
    }
}

-(void)update:(CFTimeInterval)currentTime {
    [self.gameMechanics.hero update];
    for (Enemy *enemy in self.gameMechanics.enemies) {
        [enemy update];
    }
    for (Enemy *enemy in self.gameMechanics.enemies) {
        if ([enemy.characterNode intersectsNode:self.gameMechanics.hero.characterNode]) {
            if (enemy.readyToStrike) {
                [NSTimer scheduledTimerWithTimeInterval:arc4random_uniform(3) target:enemy selector:@selector(toggleReadyToStrike) userInfo:nil repeats:NO];
                [enemy attackInDirection:enemy.currentDirection];
                enemy.readyToStrike = NO;
                if ([enemy.swordSlash intersectsNode:self.gameMechanics.hero.characterNode]) {
                    [self.gameMechanics stopCharacter:self.gameMechanics.hero];
                    [self.gameMechanics.hero takeDamage:enemy.damageDealt fromDirection:enemy.currentDirection];
                    if (self.gameMechanics.hero.isDead) {
                        [self.gameMechanics.hero removeFromParent];
                        [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(startNextLevel) userInfo:nil repeats:NO];
                    }
                }
            } else {
                NSLog(@"Not ready to strike");
            }
        }
    }
}

-(void)startNextLevel {
    [self enumerateChildNodesWithName:@"*" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
    [self removeFromParent];
    
    GameScene *nextScene = [[GameScene alloc] initWithSize:self.size];
    nextScene.scaleMode =  SKSceneScaleModeAspectFill;
    SKTransition *flip = [SKTransition flipVerticalWithDuration:0.5];
    [self.view presentScene:nextScene transition:flip];
}

-(void)debugPath:(SKSpriteNode*)node {
    SKShapeNode *shapePath = [[SKShapeNode alloc] init];
    CGPathRef pathRef = CGPathCreateWithEllipseInRect(node.frame, nil);
    shapePath.path = pathRef;
    shapePath.lineWidth = 5;
    shapePath.strokeColor = [SKColor greenColor];
    shapePath.position = CGPointMake(0, 0);
    [node addChild:shapePath];
    shapePath.zPosition = 1000;
}

@end
