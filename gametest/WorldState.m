//
//  WorldState.m
//  gametest
//
//  Created by Anders on 2015-04-23.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "WorldState.h"


@interface WorldState()



@end

@implementation WorldState

+(id)instance {
    static WorldState *instance = nil;
    if (!instance) {
        instance = [[WorldState alloc] init];
    }
    return instance;
}

-(id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}


@end
