//
//  Enemy.m
//  gametest
//
//  Created by Anders on 2015-04-09.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "Enemy.h"
#import "constants.h"

@interface Enemy ()

@property NSTimer *timer;
@property NSTimeInterval interval;


@end

@implementation Enemy

-(id)init {
    if (self = [super init]) {
        SKAction *changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"knightidleleft"]];
        [self.characterNode runAction:changeTexture];
        [self setUpProperties];
        self.physicsBody.categoryBitMask = enemyCategory;
        [self setUpHealthBar];
        [self setUpWalkActions];
        [self randomlyMoveEnemy];
    }
    return self;
}

-(void)setUpProperties {
    self.name = @"enemy";
    self.HP = 100;
    self.maxHealth = self.HP;
    self.strength = 10;
    self.toughness = 5;
    self.characterSpeed = 10;
    self.readyToStrike = YES;
}

-(void)setUpWalkActions {
    self.walkRightAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"knight" andBaseName:@"walkright" andNumberOframes:8 pingPong:NO]];
    self.walkLeftAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"knight" andBaseName:@"walkleft" andNumberOframes:8 pingPong:NO]];
    self.walkUpAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"knight" andBaseName:@"walkleft" andNumberOframes:8 pingPong:NO]];
    self.walkDownAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"knight" andBaseName:@"walkright" andNumberOframes:8 pingPong:NO]];
}

-(void)randomlyMoveEnemy {
    self.interval = (float)arc4random_uniform(4);
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.interval target:self selector:@selector(randomlyMoveEnemy) userInfo:nil repeats:NO];
    self.currentDirection = arc4random_uniform(right) + up;
    self.isMoving = !self.isMoving;
    if (self.isMoving) {
        [self animate];
    }
}

-(void)animate {
    if (self.currentDirection == left) {
        [self.characterNode runAction:self.walkLeftAction];
    } else if (self.currentDirection == right) {
        [self.characterNode runAction:self.walkRightAction];
    } else if (self.currentDirection == up) {
        [self.characterNode runAction:self.walkLeftAction];
    } else if (self.currentDirection == down) {
        [self.characterNode runAction:self.walkRightAction];
    }
}

-(void)setUpHealthBar {
    self.healthContainer = [SKSpriteNode spriteNodeWithImageNamed:@"SmallHealthContainer"];
    self.healthContainer.zPosition = 200;
    self.healthContainer.position = CGPointMake(0, (self.characterNode.frame.size.height / 2) + 5);
    [self addChild:self.healthContainer];
    self.healthBar = [SKSpriteNode spriteNodeWithImageNamed:@"SmallHealthBar"];
    self.healthBar.zPosition = self.healthContainer.zPosition + 1;
    self.healthBar.position = CGPointMake(-(self.healthBar.frame.size.width / 2), (self.characterNode.frame.size.height / 2) + 5);
    self.healthBar.anchorPoint = CGPointMake(0, 0.5);
    [self addChild:self.healthBar];
}

-(void)setChangeTextureAction {
    switch (self.currentDirection) {
        case left:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"knightidleleft"] resize:YES];
            break;
        case right:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"knightidleright"] resize:YES];
            break;
        case up:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"knightidleleft"] resize:YES];
            break;
        case down:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"knightidleright"] resize:YES];
            break;
        default:
            NSLog(@"Don't do this!");
            break;
    }
}

-(void)toggleReadyToStrike {
    self.readyToStrike = !self.readyToStrike;
    NSLog(self.readyToStrike ? @"Ready to strike" : @"Getting ready to strike");
}


@end
