//
//  Hero.m
//  gametest
//
//  Created by Anders on 2015-04-07.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "Character.h"
#import "constants.h"

@interface Character()

@end

@implementation Character

-(id)init {
    if (self = [super init]) {
        self.characterNode = [SKSpriteNode spriteNodeWithImageNamed:@"heroidleleft"];
        [self addChild:self.characterNode];
        self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.characterNode.size.width / 2];
        self.physicsBody.dynamic = YES;
        self.physicsBody.restitution = 0;
        self.physicsBody.allowsRotation = NO;
        self.physicsBody.categoryBitMask = characterCategory;
        self.physicsBody.collisionBitMask = edgeOfMapCategory | characterCategory;
        self.physicsBody.contactTestBitMask = edgeOfMapCategory | characterCategory;
        self.currentDirection = down;
        self.isDead = NO;
    }
    return self;
}

-(SKAction*)setUpFramesWithAtlasNamed:(NSString*)atlasName andBaseName:(NSString*)baseName andNumberOframes:(int)numberOfFrames pingPong:(bool)pingPong {
    NSMutableArray *textures = [NSMutableArray arrayWithCapacity:numberOfFrames];
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:atlasName];
    for (int i = 0; i < numberOfFrames; i++) {
        NSString *fullTextureName = [NSString stringWithFormat:@"%@%@0%d", atlasName, baseName, i+1];
        [textures addObject:[textureAtlas textureNamed:fullTextureName]];
    }
    if (pingPong) {
        for (int i = numberOfFrames - 1; i > 1; i--) {
            NSString *fullTextureName = [NSString stringWithFormat:@"%@%@0%d", atlasName, baseName, i];
            [textures addObject:[textureAtlas textureNamed:fullTextureName]];
        }
    }
    return [SKAction animateWithTextures:textures timePerFrame:1.0 / numberOfFrames resize:YES restore:YES]; //Don't forget to divide with FPS here!!!
}

-(void)attackInDirection:(int)direction  {
    self.swordSlash = [[SKSpriteNode alloc] initWithColor:[SKColor redColor] size:CGSizeMake(60, 60)];
    self.swordSlash.zPosition = 1000;
    [self addChild:self.swordSlash];
    [NSTimer  scheduledTimerWithTimeInterval:0.1 target:self.swordSlash selector:@selector(removeFromParent) userInfo:nil repeats:NO];
    switch (direction) {
        case left:
            self.swordSlash.position = CGPointMake(- 60, 0);
            break;
        case right:
            self.swordSlash.position = CGPointMake(60, 0);
            break;
        case up:
            self.swordSlash.position = CGPointMake(0, 80);
            break;
        case down:
            self.swordSlash.position = CGPointMake(0, - 80);
            break;
        default:
            break;
    }
    int damageToDeal = 10;
    for (int i = 0; i < self.strength; i++) {
        damageToDeal += arc4random_uniform(self.strength);
    }
    self.damageDealt = damageToDeal;
}

-(void)attack:(Character*)character  {
    self.swordSlash = [[SKSpriteNode alloc] initWithColor:[SKColor redColor] size:CGSizeMake(60, 60)];
    self.swordSlash.zPosition = 10000;
    [self addChild:self.swordSlash];
    [NSTimer  scheduledTimerWithTimeInterval:0.1 target:self.swordSlash selector:@selector(removeFromParent) userInfo:nil repeats:NO];
    switch (self.currentDirection) {
        case left:
            self.swordSlash.position = CGPointMake(- 60, 0);
            break;
        case right:
            self.swordSlash.position = CGPointMake(60, 0);
            break;
        case up:
            self.swordSlash.position = CGPointMake(0, 80);
            break;
        case down:
            self.swordSlash.position = CGPointMake(0, - 80);
            break;
        default:
            break;
    }
    if (character != nil) {
        int damageToDeal = 10;
        for (int i = 0; i < self.strength; i++) {
            damageToDeal += arc4random_uniform(self.strength);
        }
    }
}

-(void)takeDamage:(int) damageToTake fromDirection:(int) direction {
    int damageTaken = damageToTake - damageToTake * self.toughness / 10;
    self.HP -= damageTaken;
    self.healthBar.xScale = ((float)self.HP / 100) / ((float)self.maxHealth / 100);
    SKAction *bounce;
    SKAction *flashFromDamage = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.05], [SKAction fadeInWithDuration:0.05]]];
    [self.characterNode runAction:[SKAction repeatAction:flashFromDamage count:3]];
    if (direction == left) {
        bounce = [SKAction moveByX:-100 y:0 duration:0.01];
    } else if (direction == right) {
        bounce = [SKAction moveByX:100 y:0 duration:0.01];
    } else if (direction == up) {
        bounce = [SKAction moveByX:0 y:100 duration:0.01];
    } else if (direction == down) {
        bounce = [SKAction moveByX:0 y:-100 duration:0.01];
    }
    [self runAction:bounce];
    if (self.HP < 1) {
        self.isDead = YES;
    }
}

-(void)update {
    if(self.isMoving) {
        switch (self.currentDirection) {
            case left:
                self.position = CGPointMake(self.position.x - self.characterSpeed, self.position.y);
                break;
            case right:
                self.position = CGPointMake(self.position.x + self.characterSpeed, self.position.y);
                break;
            case up:
                self.position = CGPointMake(self.position.x, self.position.y + self.characterSpeed);
                break;
            case down:
                self.position = CGPointMake(self.position.x, self.position.y - self.characterSpeed);
                break;
            default:
                NSLog(@"Something went wrong!!");
                break;
        }
    } else {
        [self setChangeTextureAction];
        [self.characterNode runAction:self.changeTexture];
    }
}

-(void)setChangeTextureAction {
    switch (self.currentDirection) {
        case left:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"heroidleleft"] resize:YES];
            break;
        case right:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"heroidleright"] resize:YES];
            break;
        case up:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"heroidleleft"] resize:YES];
            break;
        case down:
            self.changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"heroidleright"] resize:YES];
            break;
        default:
            NSLog(@"Something went wrong!!");
            break;
    }
}


@end
