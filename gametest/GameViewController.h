//
//  GameViewController.h
//  gametest
//

//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

@end
