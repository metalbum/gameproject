//
//  Hero.m
//  gametest
//
//  Created by Anders on 2015-04-07.
//  Copyright (c) 2015 Anders Widén. All rights reserved.
//

#import "Hero.h"
#import "constants.h"

@implementation Hero

-(id)init {
    if (self = [super init]) {
        SKAction *changeTexture = [SKAction setTexture:[SKTexture textureWithImageNamed:@"heroidleright"]];
        [self.characterNode runAction:changeTexture];
        [self setUpProperties];
        [self setUpWalkActions];
    }
    return self;
}

-(void)setUpProperties {
    self.name = @"hero";
    self.HP = 250;
    self.maxHealth = self.HP;
    self.strength = 10;
    self.toughness = 5;
    self.characterSpeed = 10;
}

-(void)setUpWalkActions {
    self.walkRightAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"hero" andBaseName:@"walkright" andNumberOframes:8 pingPong:NO]];
    self.walkLeftAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"hero" andBaseName:@"walkleft" andNumberOframes:8 pingPong:NO]];
    self.walkUpAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"hero" andBaseName:@"walkleft" andNumberOframes:8 pingPong:NO]];
    self.walkDownAction = [SKAction repeatActionForever:[self setUpFramesWithAtlasNamed:@"hero" andBaseName:@"walkright" andNumberOframes:8 pingPong:NO]];
}

-(void)setUpHealthBar:(SKScene*)scene {
    self.healthContainer = [SKSpriteNode spriteNodeWithImageNamed:@"LargeHealthContainer"];
    self.healthContainer.zPosition = 1000;
    self.healthContainer.position = CGPointMake(20 - ((scene.frame.size.width / 2) - (self.healthContainer.frame.size.width / 2)), 630 - ((scene.frame.size.height / 2) - (self.healthContainer.frame.size.height / 2)));
    [self addChild:self.healthContainer];
    
    self.healthBar = [SKSpriteNode spriteNodeWithImageNamed:@"LargeHealthBar"];
    self.healthBar.zPosition = self.healthContainer.zPosition - 1;
    self.healthBar.position = CGPointMake(self.healthContainer.position.x -(self.healthBar.frame.size.width / 2), self.healthContainer.position.y);
    self.healthBar.anchorPoint = CGPointMake(0, 0.5);
    [self addChild:self.healthBar];
}

@end
